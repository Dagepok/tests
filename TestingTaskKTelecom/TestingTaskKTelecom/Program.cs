﻿

namespace TestingTaskKTelecom
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var db = new SQLWorker();
            db.Connect();
            //db.DropTables();
            db.CreateTables();
           
            var xlsxFilePath = args.Length == 0 ? "Контрагенты.xlsx" : args[0];
            ExcelWorker.XlsxParsing(xlsxFilePath);
            db.RetrieveData("result.xlsx");
        }


    }
}


