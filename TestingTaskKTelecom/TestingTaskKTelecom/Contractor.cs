﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using MySql.Data.MySqlClient;

namespace TestingTaskKTelecom
{
    public class Contractor : SqlConnector
    {
       
        public int Id { get; private set; }

        public string Name { get; }
        public string Type { get; }
        public string Service { get; }
        public string AddressFrom { get; }
        public string AddressTo { get; }
        public string Manager { get; }

        public Contractor(string name, string type, string service, string addressFrom, string addressTo, string manager)
        {
            Name = name;
            Type = type;
            Service = service;
            AddressTo = addressTo;
            AddressFrom = addressFrom;
            Manager = manager;
            Connect();
        }

        public Contractor(int id, string name, int typeId, int serviceId, int addressFromId, int addressToId, int managerId)
        {
            Id = id;
            Connect();
            Name = name;
            Type = GetSimpleStringFromId(typeId);
            Service = GetSimpleStringFromId(serviceId);
            AddressFrom = GetComplexStringFromId(addressFromId);
            AddressTo = GetComplexStringFromId(addressToId);
            Manager = GetSimpleStringFromId(managerId);
        }
        public override string ToString()
        {
            return string.Join(" ", GetType()
                .GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.Name != "Id")
                .Select(x => x.GetValue(this)));
        }

        #region RetrievingData




        private string GetComplexStringFromId(int id)
        {
            var table = TablesById[id / 1000000];
            string name;
            int nestedId;
            using (
                var reader =
                    new MySqlCommand($"SELECT * FROM {table} WHERE Код = {id};", Connection)
                        .ExecuteReader())
            {
                reader.Read();
                name = reader.GetString(2);
                nestedId = reader.GetInt32(1);
            }
            return table == "улицы" ? string.Join(", ", GetSimpleStringFromId(nestedId), name)
                                    : string.Join(", ", GetComplexStringFromId(nestedId), name);
        }

        private string GetSimpleStringFromId(int id)
        {
            var table = TablesById[id / 1000000];
            using (
                var reader =
                    new MySqlCommand($"SELECT * FROM {table} WHERE Код = '{id}';", Connection)
                        .ExecuteReader())
            {
                return reader.Read() ? reader.GetString(1) : null;
            }
        }

        public void ToXlsx(string xlsxFilePath)
        {
            ExcelWorker.AppendToFile(xlsxFilePath, this);
        }

        #endregion

        #region InsertingData

        private int InsertTypeAndGetId(string contractorType)
        {
            var id = FindFreeId("справочник_типов_контрагентов",
                $"SELECT * FROM справочник_типов_контрагентов WHERE название_типа = '{contractorType }';",
                0, out bool isNeedInsert);
            if (isNeedInsert)
                new MySqlCommand($"INSERT INTO справочник_типов_контрагентов (код, название_типа) values ('{id}','{contractorType}');",
                    Connection).ExecuteNonQuery();
            return id;
        }
        private int InsertCityAndGetId(string cityName)
        {
            var id = FindFreeId("города", $"SELECT * FROM города WHERE наименование = '{cityName}';", 1000000, out bool isNeedInsert);
            if (isNeedInsert)
                new MySqlCommand($"INSERT INTO города (код, наименование) values ('{id}','{cityName}');", Connection).ExecuteNonQuery();
            return id;
        }

        private int InsertStreetAndGetId(string streetName, string cityName)
        {
            var cityId = InsertCityAndGetId(cityName);
            var id = FindFreeId("улицы", $"SELECT * FROM улицы WHERE город = '{cityId}' AND наименование = '{streetName}';",
                2000000, out bool isNeedInsert);
            if (isNeedInsert)
                new MySqlCommand($"INSERT INTO улицы (код, город, наименование) values ('{id}','{cityId}','{streetName}');", Connection).ExecuteNonQuery();
            return id;
        }

        public int InsertAddressAndGetId(string address)
        {
            var addressParts = address.Split(',');
            var streetId = InsertStreetAndGetId(addressParts[1].Remove(0, 1), addressParts[0]);
            var houseNumber = addressParts[2].Remove(0, 1);
            var id = FindFreeId("адреса",
                $"SELECT * FROM адреса WHERE улица = '{streetId}' AND номер_дома = '{houseNumber}';",
                3000000, out bool isNeedInsert);
            if (isNeedInsert)
                new MySqlCommand($"INSERT INTO адреса (код, улица, номер_дома) values ('{id}','{streetId}','{houseNumber}');",
                    Connection).ExecuteNonQuery();
            return id;
        }

        private int InsertManagerAndGetId()
        {
            var id = FindFreeId("менеджеры", $"SELECT * FROM менеджеры WHERE ФИО = '{Manager}';", 4000000, out bool isNeedInsert);
            if (isNeedInsert)
                new MySqlCommand($"INSERT INTO менеджеры (код, ФИО) values ('{id}','{Manager}');", Connection).ExecuteNonQuery();
            return id;
        }

        private int FindFreeId(string tableName, string command, int defaultId, out bool isNeedInsert)
        {
            isNeedInsert = false;
            using (var reader = new MySqlCommand(command, Connection).ExecuteReader())
            {
                if (reader.Read())
                    return int.Parse(reader["Код"].ToString());

            }
            isNeedInsert = true;
            MySqlDataReader idRequest;
            using (idRequest = new MySqlCommand("SELECT MAX(Код) FROM " + tableName + ";", Connection)
                .ExecuteReader())
            {
                idRequest.Read();
                return idRequest.IsDBNull(0) ? defaultId : int.Parse(idRequest["MAX(Код)"].ToString()) + 1;
            }
        }

        public void InsertData()
        {
            var contractorTypeId = InsertTypeAndGetId(Type);
            var addressFromId = InsertAddressAndGetId(AddressFrom);
            var addressToId = InsertAddressAndGetId(AddressTo);
            var managerId = InsertManagerAndGetId();
            var serviceTypeId = InsertTypeAndGetId(Service);
            Id = FindFreeId("Контрагенты", $"SELECT код FROM контрагенты WHERE наименование = '{Name}' AND" +
                                  $" тип_контрагента = '{contractorTypeId}' AND тип_сервиса = '{serviceTypeId}' AND" +
                                  $" адрес_от = '{addressFromId}' AND адрес_до = '{addressToId}' AND" +
                                  $" Ответственный_менеджер = '{managerId}';",
                          6000000, out bool isNeedInsert);
            if (isNeedInsert)
                new MySqlCommand("INSERT INTO контрагенты (код, наименование, тип_контрагента, тип_сервиса," +
                                 " адрес_от, адрес_до, ответственный_менеджер) values" +
                                 $" ('{Id}','{Name}','{contractorTypeId}','{serviceTypeId}'," +
                                 $"'{addressFromId}','{addressToId}','{managerId}');", Connection).ExecuteNonQuery();
        }
        #endregion
    }

}