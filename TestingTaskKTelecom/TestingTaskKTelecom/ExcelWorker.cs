﻿using System;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace TestingTaskKTelecom
{
    public class ExcelWorker
    {
        public static void XlsxParsing(string path)
        {
            using (var doc = SpreadsheetDocument.Open(path, false))
            {
                var sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                var worksheet = ((WorksheetPart)doc.WorkbookPart.GetPartById(sheet.Id.Value)).Worksheet;
                var rows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
                foreach (var row in rows)
                {
                    if (row.RowIndex.Value == 1)
                        continue;
                    var rowData = row.Descendants<Cell>().Select(cell => GetCellValue(doc, cell)).ToList();
                    new Contractor(rowData[0], rowData[1], rowData[2], rowData[3], rowData[4], rowData[5]).InsertData();
                }

            }
        }
        private static string GetCellValue(SpreadsheetDocument doc, CellType cell)
        {
            var value = cell.CellValue.InnerText;
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return doc.WorkbookPart.SharedStringTablePart.SharedStringTable.ChildElements.GetItem(int.Parse(value)).InnerText;
            }
            return value;
        }

        public static void AppendToFile(string path, Contractor contractor)
        {
            if (File.Exists(path))
                using (var doc = SpreadsheetDocument.Open(path, true))
                {
                    var sheet = doc.WorkbookPart.Workbook.Sheets.GetFirstChild<Sheet>();
                    var worksheet = ((WorksheetPart)doc.WorkbookPart.GetPartById(sheet.Id.Value)).Worksheet;
                    InsertRow(worksheet.WorksheetPart, contractor);

                }
            else
            {
                using (var doc = SpreadsheetDocument.Create(path, SpreadsheetDocumentType.Workbook, true))
                {
                    CreateFileWithFirstRow(doc, contractor);
                }
            }
        }

        private static void CreateFileWithFirstRow(SpreadsheetDocument doc, Contractor contractor)
        {
            var workbookpart = doc.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();
            var worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());
            var sheets = doc.WorkbookPart.Workbook.AppendChild(new Sheets());
            var sheet = new Sheet { Id = doc.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 0, Name = "Лист 1" };
            sheets.Append(sheet);
            CreateHeader(worksheetPart);
            var columns = new Columns();
            columns.Append(new Column { Min = 1, Max = 1, Width = 30, CustomWidth = true });
            columns.Append(new Column { Min = 2, Max = 3, Width = 15, CustomWidth = true });
            columns.Append(new Column { Min = 4, Max = 5, Width = 30, CustomWidth = true });
            columns.Append(new Column { Min = 6, Max = 6, Width = 30, CustomWidth = true });
            worksheetPart.Worksheet.InsertAt(columns, 0);
            InsertRow(worksheetPart, contractor);
        }

        private static Row CreateRow(Contractor contractor, uint index)
        {
            var row = new Row { RowIndex = index };
            row.InsertAt(new Cell(new CellValue(contractor.Name)) { DataType = new EnumValue<CellValues>(CellValues.String) }, 0);
            row.InsertAt(new Cell(new CellValue(contractor.Type)) { DataType = new EnumValue<CellValues>(CellValues.String) }, 1);
            row.InsertAt(new Cell(new CellValue(contractor.Service)) { DataType = new EnumValue<CellValues>(CellValues.String) }, 2);
            row.InsertAt(new Cell(new CellValue(contractor.AddressFrom)) { DataType = new EnumValue<CellValues>(CellValues.String) }, 3);
            row.InsertAt(new Cell(new CellValue(contractor.AddressTo)) { DataType = new EnumValue<CellValues>(CellValues.String) }, 4);
            row.InsertAt(new Cell(new CellValue(contractor.Manager)) { DataType = new EnumValue<CellValues>(CellValues.String) }, 5);
            return row;
        }

        private static void CreateHeader(WorksheetPart worksheetPart)
        {
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
            var row = new Row { RowIndex = 1 };
            row.InsertAt(new Cell(new CellValue("Наименование контрагента")) { DataType = new EnumValue<CellValues>(CellValues.String) }, 0);
            row.InsertAt(new Cell(new CellValue("Тип контрагента")) { DataType = new EnumValue<CellValues>(CellValues.String) }, 1);
            row.InsertAt(new Cell(new CellValue("Тип услуги*:")) { DataType = new EnumValue<CellValues>(CellValues.String) }, 2);
            row.InsertAt(new Cell(new CellValue("Адрес VLAN  От")) { DataType = new EnumValue<CellValues>(CellValues.String) }, 3);
            row.InsertAt(new Cell(new CellValue("Адрес До")) { DataType = new EnumValue<CellValues>(CellValues.String) }, 4);
            row.InsertAt(new Cell(new CellValue("Ответственный менеджер*:")) { DataType = new EnumValue<CellValues>(CellValues.String) }, 5);
            sheetData.InsertAt(row, 0);
        }
        private static void InsertRow(WorksheetPart worksheetPart, Contractor contractor)
        {
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
            var lastRow = sheetData.Elements<Row>().LastOrDefault();
            sheetData.InsertAfter(CreateRow(contractor, lastRow.RowIndex + 1), lastRow);
        }
    }
}