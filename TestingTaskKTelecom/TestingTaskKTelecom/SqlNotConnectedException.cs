﻿using System;
using System.Net.Configuration;

namespace TestingTaskKTelecom
{
    public class SqlNotConnectedException : Exception
    {
        public override string Message { get; }

        public SqlNotConnectedException()
        {
            Message = "Connection not started";
        }
    }
}