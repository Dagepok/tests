﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace TestingTaskKTelecom
{
    public class SqlConnector
    {
        internal static readonly Dictionary<int, string> TablesById = new Dictionary<int, string>
        {
            {0, "справочник_типов_контрагентов"},
            {1, "города"},
            {2, "улицы"},
            {3, "адреса"},
            {4, "менеджеры"},
        };

        public void Connect()
        {
            var ConnectionString = "server=" + ServerName +
                ";user=" + UserName +
                ";database=" + DBName +
                ";port=" + Port +
                ";password=" + Password + ";";

            Connection = new MySqlConnection(ConnectionString);

            Connection.Open();

        }

        private string ServerName => "localhost";
        private string UserName => "root";
        private string Password => "1234";
        private string DBName => "test";
        private string Port => "3306";
        public MySqlConnection Connection { get; private set; }
    }
}