﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace TestingTaskKTelecom
{
    public class SQLWorker : SqlConnector
    {
        public void CreateTables()
        {
            if (Connection.State != ConnectionState.Open) throw new SqlNotConnectedException();
            var queries = new Dictionary<string, string>()
            {
                {
                    "Контрагенты", "create table Контрагенты(Код int, Наименование text," +
                                   " Тип_контрагента smallint,Тип_сервиса smallint, Адрес_от int, Адрес_до int, Ответственный_менеджер int);"
                },
                {"города", "create table Города(Код int, Наименование text);"},
                {"улицы", "create table Улицы(Код int, Город int, Наименование text);"},
                {"Адреса", "create table Адреса(Код int, Улица int, Номер_дома smallint);"},
                {"Менеджеры", "create table Менеджеры(Код int, ФИО text);"},
                {"Справочник_типов_контрагентов","create table Справочник_типов_контрагентов(Код smallint, Название_типа text);"}
            };
            foreach (var query in queries)
            {
                MySqlDataReader reader;
                bool isTableExist;
                using (reader = new MySqlCommand($"SHOW TABLES LIKE '{query.Key}';", Connection).ExecuteReader())
                {
                    isTableExist = reader.Read();
                }
                if (!isTableExist)
                    new MySqlCommand(query.Value, Connection).ExecuteNonQuery();
            }
        }
        public void DropTables()
        {

            if (Connection.State != ConnectionState.Open) throw new SqlNotConnectedException();
            var queries = new List<string>
            {
                "drop table Контрагенты;",
                "drop table Города;",
                "drop table Улицы;",
                "drop table Адреса;",
                "drop table Менеджеры;",
                "drop table Справочник_типов_контрагентов;"
            };
            foreach (var query in queries)
                new MySqlCommand(query, Connection).ExecuteNonQuery();
        }

        public void RetrieveData(string xlsxPath)
        {
            using (var reader = new MySqlCommand("SELECT * FROM контрагенты;", Connection).ExecuteReader())
            {
                if (!reader.HasRows) return;
                while (reader.Read())
                    new Contractor(reader.GetInt32(0), reader.GetString(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4),
                             reader.GetInt32(5), reader.GetInt32(6)).ToXlsx(xlsxPath);
            }
        }


    }


}